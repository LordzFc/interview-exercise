import React from 'react';
import Appbar from 'muicss/lib/react/appbar';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

export const ExtendedAppBar = ({text}) => {
  const style = {textAlign: 'center', marginTop: 0, marginBottom: 0, color: '#FFF'};
  const appbarStyle = {boxShadow: '0 2px 8px rgba(0, 0, 0, 0.5)', margin: 15, background: '#4e6df0'};
  return <Appbar style={appbarStyle}><Link to="/"><h1 className="mui--appbar-line-height" style={style}>{text}</h1></Link></Appbar>;
};

ExtendedAppBar.propTypes = {
  text: PropTypes.string
};