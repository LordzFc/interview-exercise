import * as types from '.././constants/actionTypes';

export const addRecord = (record) => ({
  type: types.ADD_RECORD,
  record
});

export const updateRecord = (record, idx) => ({
  type: types.UPDATE_RECORD,
  record,
  idx
});

export const removeRecord = (idx) => ({
  type: types.REMOVE_RECORD,
  idx
});