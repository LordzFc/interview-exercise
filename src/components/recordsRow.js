import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

import './recordsRow.css';


export const Record = ({record}) => {
  return (<li className='records-row'>
    <Link to={'/record/' + record.idx}>
      <span>{record.firstName  + ' ' + record.lastName}</span>
      <span className="points">{record.points}</span>
      <p>{record.date}</p>
    </Link>
  </li>);
};

Record.propTypes = {
  record: PropTypes.shape({
    firstName: PropTypes.string, 
    lastName: PropTypes.string, 
    points: PropTypes.number, 
    date: PropTypes.string, 
    idx: PropTypes.number
  }),
  no: PropTypes.number,
  removeRecord: PropTypes.func
};