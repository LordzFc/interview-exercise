import React, { Component } from 'react';
import './App.css';
import './react-datetime.css';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

import {rootReducer} from './reducers/index';
import {LeaderboardContainer} from './containers/leaderboard';
import {SingleRecordView} from './containers/singleRecord';
import {NewRecord} from './containers/addRecord';
import {EditRecord} from './containers/editRecord';


const store = createStore(
  combineReducers({
    ...rootReducer,
    routing: routerReducer
  })
);


const history = syncHistoryWithStore(browserHistory, store);



class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Route path="/" component={LeaderboardContainer} />
          <Route path="/record/create" component={NewRecord} />
          <Route path="/record/(:idx)" component={SingleRecordView} />
          <Route path="/record/(:idx)/edit" component={EditRecord} />
        </Router>
      </Provider>
    );
  }
}

export default App;
