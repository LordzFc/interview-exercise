import * as types from '.././constants/actionTypes';

export const updateSingleRecordValue  = (property, value) => ({
  type: types.UPDATE_SINGLE_RECORD_VALUE,
  property,
  value
});