import {ADD_RECORD, UPDATE_RECORD, REMOVE_RECORD} from '../constants/actionTypes';

// mock for testing
const initialState = [
  {firstName: 'Adrian', lastName: 'Z', points: 12, date: '2018-02-12', idx: 1},
  {firstName: 'Janusz', lastName: 'P', points: 15, date: '2018-02-14', idx: 2},
  {firstName: 'Andrzej', lastName: 'D', points: 17, date: '2018-02-12', idx: 3},
  {firstName: 'Hanna', lastName: 'G', points: 19, date: '2018-02-13', idx: 4}
];

export const records = (state = initialState, action) => {
  switch(action.type) {

  case ADD_RECORD:
    return [
      ...state,
      {
        ...action.record,
        idx: state.reduce((maxId, record) => Math.max(record.idx, maxId), -1) + 1
      }
    ];

  case UPDATE_RECORD:
    return state.map(record =>
      record.idx === action.idx ?
        {...action.record} :
        record
    );

  case REMOVE_RECORD:
    return state.filter(record =>
      record.idx !== action.idx
    );
  default:
    return state;
  }
};