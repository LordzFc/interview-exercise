import { connect } from 'react-redux';
import { AddEditRecord } from '.././components/addEditRecord';
import { addRecord } from '.././actions/records';
import { updateSingleRecordValue } from '.././actions/singleRecord';

const mapStateToProps = (state) => ({
  record:  state.singleRecord
});

const mapDispatchToProps = {
  save: addRecord,
  updateSingleRecordValue
};

export const NewRecord = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEditRecord);
