import React from 'react';
import PropTypes from 'prop-types';
import {ExtendedAppBar} from './extendedAppBar';
import Panel from 'muicss/lib/react/panel';


export const SingleRecordContainer = ({title, className, children}) => {
  const panelStyle = {margin: 15};
  return (<div className={className}>
    <ExtendedAppBar text={title} />
    <Panel style={panelStyle}>
      {children}
    </Panel>
  </div>);
};

SingleRecordContainer.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string
};