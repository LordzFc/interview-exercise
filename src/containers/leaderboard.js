import { connect } from 'react-redux';
import { LeaderBoard } from '.././components/leaderboard';
import { removeRecord } from '.././actions/records';

const filterRecords = (records, filter, date) => {
  switch (filter) {
    case 'FILTER_BY_DATE':
      return records.filter(r => r.date === date);
    default:
      return records;
  }
}
// sort func

const mapStateToProps = (state, ownProps) => ({
  records:  state.records.sort((a, b) => b.points -  a.points)
});

const mapDispatchToProps = {
  removeRecord
};

export const LeaderboardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LeaderBoard);

