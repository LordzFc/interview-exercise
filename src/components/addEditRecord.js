import React from 'react';
import PropTypes from 'prop-types';
import Button from 'muicss/lib/react/button';
import {SingleRecordContainer} from './singleRecordCont';
import * as Datetime from 'react-datetime'
import Input from 'muicss/lib/react/input';

export const AddEditRecord  = ({record, save, updateSingleRecordValue}) => {
  return (
    <SingleRecordContainer title="Leaderboard">
      <Input placeholder="First Name" 
        value={record.firstName}  
        onChange={(ev)=>updateSingleRecordValue('firstName', ev.target.value)} />

      <Input placeholder="Second Name" 
        value={record.lastName}
        onChange={(ev)=>updateSingleRecordValue('secondName', ev.target.value)} />
      <Input placeholder="Points" 
        value={record.points}
        onChange={(ev)=>updateSingleRecordValue('points', parseInt(ev.target.value, 10))}/>

      <Datetime inputProps={{placeholder: 'Select Date'}} />
      <Button onClick={save.bind(this, record)} style={{float: "right"}} variant="flat" color="primary"> Save </Button>
    </SingleRecordContainer>
  );
};

AddEditRecord.propTypes = {
  record: PropTypes.shape({
    firstName: PropTypes.string, 
    lastName: PropTypes.string, 
    points: PropTypes.number, 
    date: PropTypes.string, 
    idx: PropTypes.number
  }),
  save: PropTypes.func,
  updateSingleRecordValue: PropTypes.func
};