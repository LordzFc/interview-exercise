import { connect } from 'react-redux';
import { SingleRecord } from '.././components/singleRecord';
import { removeRecord } from '.././actions/records';

const mapStateToProps = (state, ownProps) => ({
  record:  state.records.find(record =>
    record.idx === parseInt(ownProps.params.idx, 10)
  )
});

const mapDispatchToProps = {
  removeRecord
};

export const SingleRecordView = connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleRecord);
