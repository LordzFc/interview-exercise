import React from 'react';
import PropTypes from 'prop-types';
import Button from 'muicss/lib/react/button';
import {Link} from 'react-router';
import {SingleRecordContainer} from './singleRecordCont';

export const SingleRecord  = ({record, removeRecord}) => {
  return (
    <SingleRecordContainer title="Leaderboard">
      <h2>{record.firstName + ' ' + record.lastName}</h2>
      <h3>Points: {record.points} </h3>
      <h3>Date: {record.date} </h3>
      <Button onClick={removeRecord.bind(this, record.idx)} variant="flat" color="danger"> Remove </Button>
      <Link to={`/record/${record.idx}/edit`}><Button variant="flat" color="danger"> Edit </Button></Link>
    </SingleRecordContainer>
  );
};

SingleRecord.propTypes = {
  record: PropTypes.shape({
    firstName: PropTypes.string, 
    lastName: PropTypes.string, 
    points: PropTypes.number, 
    date: PropTypes.string, 
    idx: PropTypes.number
  }),
  no: PropTypes.number,
  removeRecord: PropTypes.func
};