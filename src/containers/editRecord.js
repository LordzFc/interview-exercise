import { connect } from 'react-redux';
import { AddEditRecord } from '.././components/addEditRecord';
import { updateRecord } from '.././actions/records';
import { updateSingleRecordValue } from '.././actions/singleRecord';

const mapStateToProps = (state, ownProps) => {
  const record = state.records.find(record =>record.idx === parseInt(ownProps.params.idx, 10));
  return {
    record:  {...record, ...state.records.singleRecord}
  };
};

const mapDispatchToProps = {
  save: updateRecord,
  updateSingleRecordValue
};

export const EditRecord = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEditRecord);
