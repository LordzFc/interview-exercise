// import { combineReducers } from 'redux';
import {records} from './records';
import {singleRecord} from './singleRecord';

export const rootReducer = {
  records,
  singleRecord
};