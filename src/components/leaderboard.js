import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Panel from 'muicss/lib/react/panel';
import * as Datetime from 'react-datetime'
import {Link} from 'react-router';
import Button from 'muicss/lib/react/button';

import {ExtendedAppBar} from './extendedAppBar';
import {Record} from './recordsRow';

export class LeaderBoard extends Component {
  render() {
    const listStyle = {
      padding: 0,
      margin: 15
    };
    const panelStyle = {
      margin: 15
    }
    return (
      <div>
        <ExtendedAppBar text='Leaderboard' />
        {/* <Panel style={panelStyle}>
          <Datetime inputProps={{placeholder: 'Select Date'}} />
        </Panel> */}
        <ul style={listStyle}>
          {this.props.records.map((r, i)=>
            <Record record={r} key={i} removeRecord={this.props.removeRecord} />
          )}
        </ul>
        <Link to="record/create" style={{marginRight: 15, float: 'right'}}><Button>Add</Button></Link>
      </div>
    );
  }
}

LeaderBoard.propTypes = {
  records: PropTypes.arrayOf(PropTypes.shape({
    firstName: PropTypes.string, 
    lastName: PropTypes.string, 
    points: PropTypes.number, 
    date: PropTypes.string, 
    idx: PropTypes.number
  })),
  removeRecord: PropTypes.func.isRequired,
};