import {UPDATE_SINGLE_RECORD_VALUE} from '../constants/actionTypes';
const initialState = {firstName: '', lastName: '', points: null, date: '', idx: null};

export const singleRecord = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_SINGLE_RECORD_VALUE:
      return {...state, [action.property]: action.value}
    default:
      return state;
  }
}